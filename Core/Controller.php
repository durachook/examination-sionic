<?php

namespace Core;

/**
 * Base controller
 *
 * PHP version 7.0
 */
abstract class Controller
{

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $route_params = [];

    private $viewParam = [];

    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }


    protected function post($field = null, $def = null){
        if(!$field)
            return $_POST;
        return (isset($_POST[$field])) ?  $_POST[$field] : $def;
    }

    protected function get($field = null, $def = null){
        if(!$field)
            return $_GET;
        return (isset($_GET[$field])) ?  $_GET[$field] : $def;
    }

    protected function sendJson($data){
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    protected function json($field = null, $def = null){
        $data = file_get_contents('php://input');
        $json = json_decode($data, true);
        if(!$field)
            return $json;
        return (isset($json[$field])) ?  $json[$field] : $def;
    }

    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name  Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     */
    public function __call($name, $args)
    {
        $method = $name . 'Action';
        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }


    protected function render($template){
        View::renderTemplate($template, $this->viewParam);
    }

    protected function assign($field, $value){
        $this->viewParam[$field] = $value;
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before()
    {
    }



    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after()
    {
    }
}
