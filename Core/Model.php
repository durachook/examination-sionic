<?php

namespace Core;



/**
 * Base model
 *
 * PHP version 7.0
 */
abstract class Model
{
    public function __construct(){
        \DB::$host = getenv('DB_HOST');
        \DB::$user = getenv('DB_USER');
        \DB::$password = getenv('DB_PASS');
        \DB::$dbName = getenv('DB_NAME');
        \DB::query("SET NAMES UTF8");
    }

    private  function prepareSqlInsUpdate($keys){
        foreach ($keys as &$key){
            $key = "$key = VALUES($key)";
        }
        return implode(', ', $keys);
    }

    /*
        * INSERT INTO products (name, code)
        * VALUES ('key', val), ('key2', val2), ....
        * ON DUPLICATE KEY UPDATE key = VALUES(key), key = VALUES(key), ......
        * */

    public function insertOrUpdate( $table, $datas)
    {
        $keys = $values = array();
        if (isset($datas[0]) && is_array($datas[0])) {
            $var = '%ll?';
            foreach ($datas as $datum) {
                ksort($datum);
                if (!$keys) $keys = array_keys($datum);
                $values[] = array_values($datum);
            }
        } else {

            $var = '%l?';
            $keys = array_keys($datas);
            $values = array_values($datas);
        }
        $prepareSqlInsUpdate = $this->prepareSqlInsUpdate($keys);
        return \DB::query("INSERT INTO %b %lb VALUES $var ON DUPLICATE KEY UPDATE $prepareSqlInsUpdate", $table, $keys, $values);
    }
}
