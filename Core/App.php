<?php
/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 10.09.2020
 * Time: 0:36
 */

namespace Core;


class App
{
    protected static $_instance = array();
    protected $cache = [];
    protected function __construct()
    {

    }

    public static function  getInstance()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }
        return new self();
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function checkAuth(){
        if(!$this->isAuth()) {
            $this->redirect('/auth/login');
        }
    }

    public function isAuth(){
        return isset($_SESSION['user']);
    }

    public function logout(){
        unset($_SESSION['user']);
        $this->redirect('/auth/login');
    }

    public function setAuth($data){
        $_SESSION['user'] = $data;
    }

    public function checkPassword($password, $hash){
       return ($this->genPasswordHash($password) === $hash);
    }

    public function genPasswordHash($password){
        return $passwordHash = sha1(USER_SALT . $password);
    }

    public function redirect($url = '/') {
        header('Location: ' . $url,true, 301);
        exit();
    }
}


