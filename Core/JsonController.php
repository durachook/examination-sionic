<?php

namespace Core;

/**
 * Base controller
 *
 * PHP version 7.0
 */
abstract class JsonController extends Controller
{


    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct($route_params)
    {

        header('Content-Type: application/json');
        parent::__construct($route_params);
    }

    protected function send($data, $status = 200){
        http_response_code($status);
        echo json_encode($data);
        exit();
    }

    protected function json($field = null, $def = null){
        $data = file_get_contents('php://input');
        $json = json_decode($data, true);
        if(!$field)
            return $json;
        return (isset($json[$field])) ?  $json[$field] : $def;
    }

}
