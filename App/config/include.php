<?php
/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 22.05.2020
 * Time: 22:33
 */

set_time_limit(60 * 10);
ini_set('max_execution_time', 60 * 10);
ini_set('memory_limit', '1000M');
date_default_timezone_set('Europe/Moscow');

header('Content-Type: text/html; charset=utf-8');
ini_set('default_charset', 'UTF-8');
mb_internal_encoding('UTF-8');

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . '/../../');
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

require_once __DIR__ . '/const.php';

require_once __DIR__ . '/../Helpers/function.php';





