<?php
/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 01.06.2020
 * Time: 0:18
 */

namespace App\Helpers;

use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

class Helper
{


    public static function error($message, $context = array())
    {
        if (!empty($context) and !is_array($context)) {
            $str = $context;
            $context = [];
            $context[] = $str;
        }

        $logger = new Logger(LOG_PATH . '/', LogLevel::ERROR);
        $logger->error($message, $context);
    }

    public static function log($message, $context = array())
    {
        if (!empty($context) and !is_array($context)) {
            $str = $context;
            $context = [];
            $context[] = $str;
        }

        $logger = new Logger(LOG_PATH . '/', LogLevel::INFO);
        $logger->info($message, $context);
    }


    public static function processCheck($name, $unlock = false, $timeout = 60 * 15)
    {
        $filePath = LOG_PATH . '/lockProcess.log';
        if (!file_exists($filePath)) {
            file_put_contents($filePath, json_encode([]));
        }
        $file = file_get_contents($filePath);
        $data = json_decode($file, true);
        if (isset($data[$name])) {
            if ($data[$name]['locked'] > 0) {
                $diffTime = time() - $data[$name]['time'];
                if ($diffTime > $timeout) {
                    $data[$name]['time'] = time();
                    $status = 1;
                } else {
                    $status = 0;
                }
            } else {
                $data[$name] = [
                    'time' => time(),
                    'locked' => 1
                ];
                $status = 1;
            }
        } else {
            $data[$name] = [
                'time' => time(),
                'locked' => 1
            ];
            $status = 1;
        }
        if ($unlock == true) {
            $data[$name] = [
                'time' => time(),
                'locked' => 0
            ];
            $status = 1;
        }
        file_put_contents($filePath, json_encode($data, JSON_PRETTY_PRINT));
        return $status;
    }

}