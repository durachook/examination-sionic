<?php

use App\Helpers\Debug;

/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 23.05.2020
 * Time: 2:48
 */


function dd()
{
    array_map(function ($x) {
        Debug::var_dump($x);
    }, func_get_args());
    die;
}

function d()
{
    array_map(function ($x) {
        Debug::var_dump($x);
    }, func_get_args());
}


function convert($size)
{
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}

function strtr_unicode($str, $a = null, $b = null)
{
    $translate = $a;
    if (!is_array($a) && !is_array($b)) {
        $a = (array)$a;
        $b = (array)$b;
        $translate = array_combine(
            array_values($a),
            array_values($b)
        );
    }
    // again weird, but accepts an array in this case
    return strtr($str, $translate);
}

function normilizeStr($string)
{
    $converter = array(
        'Ё' => 'Е',
        'ё' => 'е',
        'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
        'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
        'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
        'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
        'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
        'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
    );

    return strtr_unicode($string, $converter);
}


function translit($s, $div = '-')
{
    $s = (string)$s; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
    $s = str_replace([' ', '-'], $div, $s); // заменяем пробелы знаком минус
    return $s; // возвращаем результат
}


function moveToArchive($sourceFile = '')
{
    if ($sourceFile == '' || !file_exists(PATH_DAV_PUBLIC . '/' . $sourceFile))
        return false;
    $date = date('dmY');
    $newDir = PATH_DAV_PUBLIC . '/archive/' . $date;
    if (!file_exists($newDir))
        mkdir($newDir, 0777, true);
    if (file_exists($newDir . '/' . $sourceFile)) {
        rename(PATH_DAV_PUBLIC . '/' . $sourceFile, $newDir . '/' . time() . '_' . $sourceFile);
    } else {
        rename(PATH_DAV_PUBLIC . '/' . $sourceFile, $newDir . '/' . $sourceFile);
    }

}


function _get($obj, $path, $defVal = null)
{
    if (is_scalar($obj)) return $obj;
    if (!is_array($obj)) $obj = json_decode(json_encode($obj), true);
    return array_reduce(array_filter(explode('.', $path)), function ($acc, $p) use ($defVal) {
        return isset($acc[$p]) ? $acc[$p] : $defVal;
    }, $obj);
}


function _set(&$obj, $path, $value)
{
    if (is_scalar($obj))
        return false;
    $path = is_array($path) ? $path : array_filter(explode('.', $path));
    $cur = &$obj;
    foreach ($path as $p) {
        if (!isset($cur[$p]))
            $cur[$p] = [];
        $cur = &$cur[$p];
    }
    $cur = $value;
}