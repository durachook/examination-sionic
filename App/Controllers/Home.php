<?php

namespace App\Controllers;

use App\Models\Products;
use App\Service\ParseProcess;
use Core\Controller;
use JasonGrimes\Paginator;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $productsModel = new Products();
        $page = intval(($this->get('page', 0) > 0) ? $this->get('page') : 1);
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $count = $productsModel->count(); // Count of all available posts
        $paginator = new Paginator($count, $limit, $page, '/?page=(:num)');
        ob_start();
        include ROOT . '/App/Views/Home/pager.phtml';
        $pagination = ob_get_contents();
        ob_end_clean();

        $products = $productsModel->getList([
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $this->assign('products', $products);
        $this->assign('pagination', $pagination);
        $this->render('Home/index.html');

    }

}
