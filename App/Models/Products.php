<?php

namespace App\Models;

use Core\Model;
use DB;
use MeekroDBException;

class Products extends Model
{

    public function __construct()
    {
        parent::__construct();
        DB::$error_handler = '';
        DB::$throw_exception_on_error = true;
    }

    public function count()
    {
        return DB::queryFirstField('SELECT count(*) as count FROM products');
    }

    public function getList($param)
    {
        $products = DB::query('SELECT * FROM products ORDER BY id DESC LIMIT %d, %d ', $param['offset'], $param['limit']);
        if (!$products) return false;
        $cityNames = $this->getCityNames();

        foreach ($products as &$product) {
            foreach ($product as $field => $value) {
                if (!isset($cityNames[$field]))
                    continue;
                if (strpos($field, 'price_') !== false) {
                    $product['price'][$cityNames[$field]] = $value;
                }
                if (strpos($field, 'quantity_') !== false) {
                    $product['quantity'][$cityNames[$field]] = $value;

                }
            }
            if (!empty($product['prod_usage'])) {
                $usagesList = explode(';;', $product['prod_usage']);
                $usagesList = array_map(function ($usage) {
                    @list($brand, $model, $cat) = explode('-', $usage);
                    return [
                        'Марка' => $brand,
                        'Модель' => $model,
                        'КатегорияТС' => $cat,
                    ];
                }, $usagesList);
                $product['usage'] = $usagesList;
            }

        }

        return $products;


    }

    public function getCityNames()
    {
        $raw = DB::query('SELECT * FROM city_names');
        $result = [];
        foreach ($raw as $item) {
            $result['price_' . $item['field']] = $item['name'];
            $result['quantity_' . $item['field']] = $item['name'];
        }
        return $result;
    }

    public function addColCityPrice($city)
    {
        return self::addColCity('price', $city);
    }

    public function addColCityQuantity($city)
    {
        return self::addColCity('quantity', $city);
    }

    private function addColCity($type, $city)
    {
        $fieldName = strtolower(translit($city, '_'));
        $colName = $type . '_' . $fieldName;
        try {
            DB::queryFirstRow("SELECT  {$colName}  FROM products WHERE 0");
        } catch (MeekroDBException $ex) {
            DB::insertUpdate('city_names', ['name' => $city, 'field' => $fieldName]);
            DB::query("ALTER TABLE `products` ADD {$colName} FLOAT NOT NULL DEFAULT '0' ;");
        }
        return $colName;
    }

    public function insertTask()
    {
        return DB::insert('tasks', ['task' => 'load_from_ms_day', 'timestamp' => time()]);
    }

    public function replace($data)
    {
        DB::replace('products', $data);
    }


    public function insertUpdate($datas)
    {
        try {
            $this->insertOrUpdate('products', $datas);
        } catch (MeekroDBException $ex) {
            var_dump($ex);
        }

    }
}
