<?php

namespace App\Command;

use App\Helpers\Helper;
use App\Service\ParseProcess;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{
    protected static $defaultName = 'process:import';

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = microtime(1);
        $timeout = 60 * 5;
        if (!Helper::processCheck('import_process', false, $timeout)) return Command::FAILURE;
        try {
            $files = array_filter(scandir(PATH_XML_DATA . '/'), function ($item) {
                return (is_file(PATH_XML_DATA . '/' . $item));
            });
            $parcer = new ParseProcess();
            foreach ($files as $file) {
                if (strpos($file, '.xml') === false)
                    continue;
                if (strpos($file, 'import') !== false) {
                    $parcer->setFile(PATH_XML_DATA . '/' . $file);
                    $parcer->processImport();
                } elseif (strpos($file, 'offers') !== false) {
                    $parcer->setFile(PATH_XML_DATA . '/' . $file);
                    $parcer->processOffers();
                }
                $parcer->moveParsed(PATH_XML_DATA , $file, 'parsed');
            }
            $output->writeln('Время выполнения:');
            $output->writeln(microtime(1) - $t1);
            Helper::processCheck('import_process', true, $timeout);
            return Command::SUCCESS;
        } catch (Exception $ex) {
            $output->writeln('Возникла ошибка:');
            $output->writeln($ex->getMessage());
            Helper::processCheck('import_process', true, $timeout);
            return Command::FAILURE;
        }


    }
}