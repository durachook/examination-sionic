<?php
/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 18.09.2020
 * Time: 13:13
 */

namespace App\Service;


use App\Models\Products;
use ArtemsWay\Parser1C\Parser1C;
use ArtemsWay\Parser1C\Parsers\DOM\ImportParser;
use Prewk\XmlStringStreamer;

class ParseProcess extends ParserXml
{

    private function getCity($str)
    {
        if (preg_match('/\((.+?)\)/', $str, $match)) {
            return $match[1];
        }
        return false;
    }

    public function moveParsed($sourceDir, $sourceFile, $toPath = '')
    {
        if (!file_exists($sourceDir . '/' . $sourceFile))
            return false;
        $date = date('dmY');
        $newDir = $sourceDir . '/' . $toPath . '/' . $date;

        if (!is_dir($newDir))
            mkdir($newDir, 0777, true);

        if (file_exists($newDir . '/' . $sourceFile)) {
            rename($sourceDir . '/' . $sourceFile, $newDir . '/' . time() . '_' . basename($sourceFile));
        } else {
            rename($sourceDir . '/' . $sourceFile, $newDir . '/' . $sourceFile);
        }
    }

    /*
     * Здесь будет проходить основной процесс  парсинга и обновление базы данных
     * Так как файлы большие и структура не стандартная,
     * я решил не пользоваться готовыми скриптами для парсинга CommercMl
     * Я использую библиотеку для потокового парсинга, скрипт читает файл блоками и парсит, так потребление памяти в десятки раз меньше
     * Так же обновление и запись в базу идет пакетами, так чуть мы увеличиваем потребление памяти, но скорость записи в сотни раз вырастает
     * */

    public function processImport()
    {

        if (!$importFile = $this->getFilePath())
            return false;
        $productsModel = new Products();
        $streamer = XmlStringStreamer::createUniqueNodeParser($importFile, array("uniqueNode" => "Товар"));
        $batchCount = 0;
        $batchLimit = 3000;
        $batchData = [];
        while ($node = $streamer->getNode()) {
            if (!$node) break;
            $batchCount++;
            $xml = simplexml_load_string($node);
            $data = [];
            $data['name'] = substr((string)$xml->Наименование, 0, 254);
            $data['code'] = (int)$xml->Код;
            $data['weight'] = (int)$xml->Вес;
            $data['prod_usage'] = '';
            $usages = [];
            if (isset($xml->Взаимозаменяемости)) {
                $usageList = $this->decode($xml->Взаимозаменяемости);
                $usageList = $usageList['Взаимозаменяемость'];
                if (isset($usageList[0]) and is_array($usageList[0])) {
                    foreach ($usageList as $usage) {
                        foreach ($usage as &$item) {
                            if (empty($item))
                                $item = '_';
                            $item = str_replace('-', '_', $item);
                        }
                        $usages[] = implode('-', $usage);
                    }
                } else {
                    foreach ($usageList as &$item) {
                        if (empty($item))
                            $item = '_';
                        $item = str_replace('-', '_', $item);
                    }
                    $usages[] = implode('-', $usageList);
                }
                $data['prod_usage'] = implode(';;', $usages);
            }
            // будем вставлять/обновлять записи в БД пакетами
            $batchData[] = $data;
            if ($batchCount >= $batchLimit) {
                $productsModel->insertUpdate($batchData);
                $batchData = [];
                $batchCount = 0;
            }
        }
        //остаточный пакет
        if ($batchData) $productsModel->insertUpdate($batchData);
    }

    public function processOffers()
    {
        if (!$importFile = $this->getFilePath())
            return false;
        $productsModel = new Products();
        $classField = $this->getSingleBlock('Классификатор');
        $city = $this->getCity((string)$classField->Наименование);
        $priceCol = $productsModel->addColCityPrice($city);
        $quantityCol = $productsModel->addColCityQuantity($city);
        $streamer = XmlStringStreamer::createUniqueNodeParser($importFile, array("uniqueNode" => "Предложение"));
        $batchCount = 0;
        $batchLimit = 3000;
        $batchData = [];
        while ($node = $streamer->getNode()) {
            if (!$node) break;
            $batchCount++;
            $xml = simplexml_load_string($node);
            $price = (int)$xml->Цены->Цена->ЦенаЗаЕдиницу;
            $quantity = (int)$xml->Количество;
            $data = [];
            $data['name'] = (string)$xml->Наименование;
            $data['code'] = (string)$xml->Код;
            $data[$priceCol] = (!empty($price)) ? $price : 0;
            $data[$quantityCol] = (!empty($quantity)) ? $quantity : 0;
            // будем вставлять/обновлять записи в БД пакетами
            $batchData[] = $data;
            if ($batchCount >= $batchLimit) {
                $productsModel->insertUpdate($batchData);
                $batchData = [];
                $batchCount = 0;
            }
        }
        if ($batchData) $productsModel->insertUpdate($batchData);
    }
}