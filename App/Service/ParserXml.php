<?php
/**
 * Created by PhpStorm.
 * User: Ruslan_Ibragimov
 * Date: 18.09.2020
 * Time: 14:24
 */

namespace App\Service;


use Prewk\XmlStringStreamer;

class ParserXml
{
    protected $filePath = null;

    protected function decode($obj)
    {
        return json_decode(json_encode($obj, JSON_UNESCAPED_UNICODE), TRUE);
    }

    public function setFile($filepath)
    {
        $this->filePath = $filepath;
    }

    protected function getFilePath()
    {
        return (file_exists($this->filePath)) ? $this->filePath : false;
    }

    public function getSingleBlock($blockName)
    {
        $streamer = XmlStringStreamer::createUniqueNodeParser($this->getFilePath(), array("uniqueNode" => $blockName));
        $node = $streamer->getNode();
        return $simpleXmlNode = simplexml_load_string($node);
    }
}