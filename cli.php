#!/usr/bin/env php
<?php
require __DIR__.'/vendor/autoload.php';
include __DIR__ . '/App/config/include.php';
use Symfony\Component\Console\Application;
$application = new Application();
$application->add(new \App\Command\ImportCommand());

$application->run();