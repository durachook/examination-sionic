<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
/**
 * Front controller
 *
 * PHP version 7.0
 */
session_start();

//define('ROOT', __DIR__ . '../' );
use Core\App;

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';

include __DIR__ . '/../App/config/include.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

function app(){return App::getInstance();}

/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('{controller}/{action}');
    
$router->dispatch($_SERVER['QUERY_STRING']);
