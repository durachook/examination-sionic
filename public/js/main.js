format = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}

var date = new Date();
var yerstoday = date.setDate(date.getDate() - 1);


var vue = new Vue({
    delimiters: ['${', '}'],
    el: '#root',
    data() {
        return {
            info: null,
            loading: false,
            dateFrom: format(new Date(yerstoday), 'yyyy-MM-dd'),
            dateTo: format(new Date(), 'yyyy-MM-dd'),
            dateMove: format(new Date(), 'yyyy-MM-dd'),
            timeMove: '09:00',
            descMove: null,
            file:'',
            selectStore: null,
            selectOrg: null,
            storeList: {},
            orgList: {},
            buttonDisabled: false

        };
    },
    self:this,
    mounted() {
        if(typeof stores !== 'undefined') {
            this.storeList = stores
            this.selectStore = this.storeList[0].uuid
        }
        if(typeof organizations !== 'undefined') {
            this.orgList = organizations
            this.selectOrg = this.orgList[0].uuid
        }

    },
    methods: {
        toggleButtons(){
            this.loading = !this.loading
            this.buttonDisabled = !this.buttonDisabled
        },
        submitFile(){
            this.toggleButtons()
            let formData = new FormData();
            formData.append('file', this.file);
            formData.append('dateMove', this.dateMove);
            formData.append('timeMove', this.timeMove);
            formData.append('selectStore', this.selectStore);
            formData.append('selectOrg', this.selectOrg);
            formData.append('descMove', this.descMove);
            axios.post('/api/moveStore', formData,
                {
                    headers: {'Content-Type': 'multipart/form-data'}
                })
                .then((response) => {
                    const classToast = (response.data.message.warning) ? 'redToast' : '';
                    if(Array.isArray(response.data.message.message) || typeof response.data.message.message === 'object'){
                        response.data.message.message.map((item) => {
                            M.toast({html: item, classes: classToast})
                        })
                    }
                    this.toggleButtons()
                })
                .catch((error) => {
                    M.toast({html: 'Ошибка' })
                    if(error.response) M.toast({html: error.response.data.message })
                    this.toggleButtons()
                });
        },

        saveDbf() {
            this.toggleButtons()
            const str = JSON.stringify({
                'dateFrom':this.dateFrom,
                'dateTo':this.dateTo,
            });
            axios.post('/api/saveDbf', str)
                .then((response) => {
                    if(response.data.status === 'ok'){
                        M.toast({html: 'Данные выгружены'})
                    } else  {
                        M.toast({html: 'Ошибка выгрузки' })
                        M.toast({html: response.data.message })
                    }
                    this.toggleButtons()
                })
                .catch((error) => {
                    console.log(error.response);
                    M.toast({html: 'Ошибка сервера' })
                    this.toggleButtons()
                });
        },
        addTask() {
            this.toggleButtons()
            axios.post('/api/todayTask', '')
                .then((response) => {
                    if(response.data.status === 'ok'){
                        M.toast({html: response.data.message })
                    }
                    this.toggleButtons()
                })
                .catch((error) => {
                    M.toast({html: 'Ошибка сервера' })
                    this.toggleButtons()
                });
        },

        handleFileUpload(){
            this.file = this.$refs.file.files[0];
        },
        change(event){
            this[event.target.name] = event.target.value
        }

    }
});